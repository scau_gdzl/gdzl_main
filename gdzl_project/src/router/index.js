import Vue from 'vue'
import Router from 'vue-router'
import scau_index from '@/components/scau_index'
import data_display from '@/components/data_display'
import data_upload from '@/components/data_upload'
import data_handle from '@/components/data_handle'
import generate_rule from '@/components/generate_rule'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'scau_index',
      component: scau_index
    },
    {
      path: '/data_display',
      name: 'data_display',
      component: resolve => require(['../components/data_display.vue'], resolve),
      meta: { title: '数据浏览'}
    },
    {
      path: '/data_upload',
      name: 'data_upload',
      component: resolve => require(['../components/data_upload.vue'], resolve),
      meta: { title: '数据上传' }
    },
    {
      path: '/data_handle',
      name: 'data_handle',
      component: resolve => require(['../components/data_handle.vue'], resolve),
      meta: { title: '数据分析' }
    },
    {
      path: '/generate_rule',
      name: 'generate_rule',
      component: generate_rule
    },


  ],
  base:"/dist/",
  mode: "hash"
})
