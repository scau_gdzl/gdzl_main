// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import axios from './axios/index.js';
import 'element-ui/lib/theme-chalk/index.css';
import '../static/css/icon.css';

Vue.config.productionTip = false;
Vue.prototype.$ELEMENT = { size: 'medium', zIndex: 3000 };
Vue.use(ElementUI);
Vue.use(ElementUI, { size: 'small', zIndex: 3000 });
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  components: { App },
  template: '<App/>'
});
